import os

dockerFileName = "docker-compose.yml"
dockerPath = "/home/server/Docker"
dockerContainerList = []

def dockerCompose(state):
    for container in dockerContainerList:
        os.system(f"docker-compose --file {dockerPath}/{container}/{dockerFileName} {state}")

print("Shutdown docker container...\n")
dockerCompose(state="down")
print("Done\n")
os.system("/bin/bash /home/server/pyBackup/backup.sh")
print("Start docker container...\n")
dockerCompose(state="up -d")
print("Done\n")

