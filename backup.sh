#!/bin/bash
clear

disk=/dev/sdb
src=/home/server
des=/home/server/.backup

sudo mount $disk /home/server/.backup

sudo rsync -auv --progress --stats --exclude .backup $src $des
sudo umount /home/server/.backup
